# NYTimes
A simple android app to hit the NY Times Most Popular Articles API and show a list of articles, that shows details when items on the list are tapped (a typical master/detail app).

## Developed Features
1. Model�view�presenter (MVP) architectural pattern
2. Recyclerview
3. Offline supported
4. Data Binding
5. Retrofit2 for server call 
6. NDK/C++ to secure API-KEY & URLS
7. UI & functions tested using JUnit & espress

## Contributing
All pull requests are welcome, make sure to follow the contribution guidelines when you submit pull request.

1. Fork it!
2. Checkout the development branch: git checkout development
3. Create your feature branch: git checkout -b my-new-feature
4. Add your changes to the index: git add .
5. Commit your changes: git commit -m 'Add some feature'
6. Push to the branch: git push origin my-new-feature
7. Submit a pull request against the development branch

## Hint
I hoped I hade made it with kotlin programming language but required task mentioned either Android (Java) or Swift. 
I hope to get your admiration.