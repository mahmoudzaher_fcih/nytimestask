package com.ceylonapz.nytimes.presenter;

import android.content.Context;

import com.ceylonapz.nytimes.R;
import com.ceylonapz.nytimes.model.apiresponses.BaseResponse;
import com.ceylonapz.nytimes.model.apiresponses.News;
import com.ceylonapz.nytimes.networkhandler.ApiClientDispatcher;
import com.ceylonapz.nytimes.networkhandler.ApiInterface;
import com.ceylonapz.nytimes.utility.Constant;
import com.ceylonapz.nytimes.view.IView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsPresenterImpl implements NewsPresenter {

    IView view;
    Context mcontext;


    public NewsPresenterImpl(IView view, Context mcontext) {
        this.view = view;
        this.mcontext = mcontext;
    }

    @Override
    public void callNewsApi(String period) {
        ApiInterface apiService = ApiClientDispatcher.getClient().create(ApiInterface.class);
        Call<BaseResponse> call = apiService.getNewsDetails(period, Constant.API_KEY);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                int statusCode = response.code();

                if (statusCode == 200) {
                    List<News> newsList = response.body().getResults();
                    view.saveNewsData(newsList);
                } else {
                    view.showError(mcontext.getResources().getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                view.showError(t.getMessage());
            }
        });
    }
}
