package com.ceylonapz.nytimes.presenter;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.ceylonapz.nytimes.model.apiresponses.News;
import com.ceylonapz.nytimes.utility.Constant;
import com.ceylonapz.nytimes.view.activities.DetailsActivity;


public class NewsClickHandler {

    private final Context context;

    public NewsClickHandler(Context context) {
        this.context = context;
    }

    public void onSaveClick(View view, News news) {
        Intent nextInt = new Intent(view.getContext(), DetailsActivity.class);
        nextInt.putExtra(Constant.SELECTEDNEWS,news);
        context.startActivity(nextInt);
    }


}
