package com.ceylonapz.nytimes.presenter;

public interface NewsPresenter {
    void callNewsApi(String period);
}
