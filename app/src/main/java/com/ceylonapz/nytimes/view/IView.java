package com.ceylonapz.nytimes.view;

import com.ceylonapz.nytimes.model.apiresponses.News;

import java.util.List;

public interface IView {

    void showError(String message);

    void dataFound();

    void setupRecycler(List<News> dataList);

    void saveNewsData(List<News> newsList);

    void getSavedNews();

}
