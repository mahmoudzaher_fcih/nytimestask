package com.ceylonapz.nytimes.view.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ceylonapz.nytimes.NewsApplication;
import com.ceylonapz.nytimes.R;
import com.ceylonapz.nytimes.model.apiresponses.News;
import com.ceylonapz.nytimes.presenter.NewsPresenterImpl;
import com.ceylonapz.nytimes.utility.Util;
import com.ceylonapz.nytimes.view.IView;
import com.ceylonapz.nytimes.view.adapters.NewsAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class MainActivity extends AppCompatActivity implements IView {

    private TextView noDataTv;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private Type listType;
    private Gson gson;
    NewsPresenterImpl presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inits();
        presenter = new NewsPresenterImpl(this, MainActivity.this);

        if (Util.isOnline(this)) {
            /*
             * for testing API, you can change the period section of the path
             * (available period values are 1,7 and 30
             */
            presenter.callNewsApi("7");

        } else {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.no_connection), Toast.LENGTH_LONG).show();
            getSavedNews();
        }
    }


    @Override
    public void showError(String message) {
        noDataTv.setText(message);
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        noDataTv.setVisibility(View.VISIBLE);
    }

    @Override
    public void dataFound() {
        noDataTv.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setupRecycler(List<News> dataList) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(new NewsAdapter(this, dataList));

        assert dataList != null;
        if (dataList.size() > 0) {
            dataFound();
        } else {
            showError(getResources().getString(R.string.no_news));
        }
    }

    private void inits() {
        listType = new TypeToken<List<News>>() {
        }.getType();
        gson = new Gson();

        progressBar = findViewById(R.id.progressBar);
        noDataTv = findViewById(R.id.textViewNoData);
        recyclerView = findViewById(R.id.recycler_news);
    }


    @Override
    public void saveNewsData(List<News> newsList) {
        //LIST DATA CONVERT TO GSON STRING
        String gsonStr = gson.toJson(newsList, listType);

        //SAVE IN SHARED-PREFERENCE
        NewsApplication.setNewsList(this, gsonStr);
        getSavedNews();

    }

    @Override
    public void getSavedNews() {
        //GET SAVED DATA
        String gsonList = NewsApplication.getNewsList(this);
        if (!gsonList.equals("n/a")) {
            //CONVERT TO LIST
            List<News> newsList = gson.fromJson(gsonList, listType);

            setupRecycler(newsList);
        } else {
            showError(getResources().getString(R.string.no_saved_news));
        }
    }
}
