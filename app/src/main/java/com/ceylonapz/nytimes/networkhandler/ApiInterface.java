package com.ceylonapz.nytimes.networkhandler;

import com.ceylonapz.nytimes.model.apiresponses.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("mostpopular/v2/viewed/{period}.json")
    Call<BaseResponse> getNewsDetails(@Path("period") String period,
                                      @Query("api-key") String apiKey);
}
